package com.ivanytskyi.model;

public class Jawelry  {
    public String name;
    public int cost;
    public int weight;
    public int transmittance;

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }

    public int getWeight() {
        return weight;
    }

    public int getTransmittance() {
        return transmittance;
    }
}
