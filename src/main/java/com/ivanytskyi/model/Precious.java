package com.ivanytskyi.model;

public class Precious extends Jawelry {
    public Precious(String name, int cost, int weight, int transmittance) {
        this.name = name;
        this.cost = cost;
        this.weight = weight;
        this.transmittance = transmittance;
    }

    @Override
    public String toString() {
        return "Precious{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", weight=" + weight +
                ", transmittance=" + transmittance +
                '}';
    }
}
