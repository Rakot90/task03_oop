package com.ivanytskyi.model;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Logic implements Model {
    private Domain domain;

    public Logic() {
        domain = new Domain();
    }

    @Override
    public Stream<Jawelry> getStream(int x) {
        return domain.getStream(x);
    }

    @Override
    public ArrayList<Jawelry> getArrayList() {
        return domain.getArrayList();
    }

    @Override
    public ArrayList<Precious> getPreciousList() {
        return domain.getPreciousList();
    }

    @Override
    public ArrayList<SemiPrecious> getSemiPreciousList() {
        return domain.getSemiPreciousList();
    }

    @Override
    public Integer getSumCost() {
        return domain.getSumCost();
    }
    @Override
    public Integer getSumWeight() {
        return domain.getSumWeight();
    }
}
