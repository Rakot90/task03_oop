package com.ivanytskyi.model;

public class SemiPrecious extends Jawelry {
    public SemiPrecious(String name, int cost, int weight, int transmittance) {
        this.name = name;
        this.cost = cost;
        this.weight = weight;
        this.transmittance = transmittance;
    }

    @Override
    public String toString() {
        return "SemiPrecious{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", weight=" + weight +
                ", transmittance=" + transmittance +
                '}';
    }
}
