package com.ivanytskyi.view;

import com.ivanytskyi.controller.Controller;
import com.ivanytskyi.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    public int ignore;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();

        menu.put("1", "  1 - Список всех драгоценных камней сортирован по стоимости");
        menu.put("2", "  2 - Список драгоценных камней");
        menu.put("3", "  3 - Список полудрагоценных камней");
        menu.put("4", "  4 - Вывести общую стоимость драгоценностей");
        menu.put("5", "  5 - Вывести общую массу камней в каратах");
        menu.put("6", "  6 - Отобразить драгоценности согласно прозрачности");
        menu.put("Q", "  Q - Выход");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        System.out.println(controller.getArrayList());
    }

    private void pressButton2() {
        System.out.println(controller.getPreciousList());
    }

    private void pressButton3() {
        System.out.println(controller.getSemiPreciousList());
    }

    private void pressButton4() {
        System.out.println("Стоимость " + controller.getSumCost() + " USD");
    }

    private void pressButton5() {
        System.out.println("Масса " + controller.getSumWeight() + " карат:");
    }

    private void pressButton6() {
        try {
            System.out.print("Введите степень прозрачности(1, 2 или 3): ");
            int x = Integer.parseInt(input.nextLine());
            System.out.println(controller.getStream(x));
        }catch(NumberFormatException e){
            System.out.println("Number format exception");
        }
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nМеню:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Виберіть пункт меню.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}